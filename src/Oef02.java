/*
 * 10 getallen worden ingelezen, druk het product van deze 10 getallen af.
 */
public class Oef02 {

	public static void main(String[] args) {
		int getal, som = 1, teller = 1;
		getal = Invoer.leesInt("Geef een getal in: ");
		while (teller != 10) {
			som = som * getal;
			teller += 1;
			getal = Invoer.leesInt("Geef een getal in: ");
		}
		System.out.println("Het product van deze 10 getallen is: " + som);
	}

}