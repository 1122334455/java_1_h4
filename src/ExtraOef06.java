/*
 * Een onderneming wenst aan haar personeelsleden een premie uit te betalen die in verhouding staat tot het aantal dienstjaren. Bereken de individuele premie en de totale premie.
 * Per personeelslid wordt dit via het toetsenbrod ingegeven: achternaam (stopt bij / of *), voornaam en aantal dienstjaren. Die een invoercontrole op het aantal dienstjaren (tussen 0 en 40).
 * De premie wordt als volgt berekent: dienstjaren * 25 euro. Minder dan 5 dienstjaren = 0 euro.
 * Druk de familienaam, voornaam, aantal jaren dienst, premie af. 
 * Druk op het einde ook nog de totale premie van iedereen samen af.
 */
public class ExtraOef06 {

	public static void main(String[] args) {
		String achternaam, voornaam;
		int dienstJaren, premie, totPremie = 0, grootst = 0;
		achternaam = Invoer.leesString("Geef de achternaam in: ");
		while (!(achternaam.equals("/") || achternaam.equals("*"))) {
			voornaam = Invoer.leesString("Geef de voornaam in: ");
			dienstJaren = Invoer.leesInt("Geef het aantal dienstjaren in: ");
			while (dienstJaren < 0 || dienstJaren > 40) {
				System.out.println("Foutieve ingave!");
				dienstJaren = Invoer
						.leesInt("Geef het aantal dienstjaren in: ");
			}
			if (dienstJaren < 5) {
				premie = 0;
			}else {
				premie = dienstJaren * 25;
			}
			if (premie > grootst) {
				grootst = premie;
			}
			totPremie += premie;
			System.out.println("Achternaam: " + achternaam + "\t" + "Voornaam: " + voornaam + "\t" + "Dienstjaren: " + dienstJaren + "\t" + "Premie: " + premie);
			achternaam = Invoer.leesString("Geef de achternaam in: ");
		}
		System.out.println("Totale premie:\t" + totPremie + "\t" + "Hoogste premie:\t" + grootst);

	}

}
