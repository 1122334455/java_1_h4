/*
 * Als de bevolking met een bepaald percentage per jaar aangroeit, na hoeveel jaar zal zij dan verdubbeld zijn?
 * De formule is: P = P0 * (1 + R)^T
 * P = huidige bevolkingsaantal, P0 = oorspronkelijk bevolkingsaantal, R = bevolkingsgroei in %, T = aantal jaren
 */
public class ExtraMOef03 {

	public static void main(String[] args) {
		int jaren = 0;
		double perc, factor = 1;
		perc = Invoer.leesDouble("Geef het groeipercentage in: ") / 100;
		while (factor < 2) {
			jaren++;
			//factor = 1;
			for (int i = 0; i <= jaren; i++) {
				factor *= (1 + perc);
			}
			/*factor = Math.pow((1 + r), ++t);*/
			// System.out.println(factor);
		}
		System.out.println("Het duurt " + jaren
				+ " jaar vooraleer de bevolking verdubbeld is.");
	}

}

/* 	Huidig bevolkingsaantal (p) en Oorspronkelijk bevolkingsaantal (p0) zijn niet nodig in principe, enkel de factor wordt berekend. 
	20% bv: 1 -> 1.2 -> 1.44 -> 1.728 -> 2.0736 => 4x => 4 jaar dus.
	Wanneer de factor 2 wordt, is de factor dus verdubbeld. Het aantal machten (lees: jaren) dat nodig is om deze te verdubbelen wordt door de loop gedaan.
*/