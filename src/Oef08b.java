/*
 * Druk de tafels van vermenigvuldigen af (tot x10) van de opeenvolgende natuurlijke getallen van 21 tot en met 30.
 */
public class Oef08b {
    public static void main(String[] args) {
        int getal, tafel;
        for (tafel = 1; tafel <= 10; tafel += 1) {
            for (getal = 21; getal <= 30; getal += 1) {
                System.out.print("\t" + getal * tafel);
            }
            System.out.print('\n');
        }
    }
}