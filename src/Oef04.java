/*
 * In de chocolaterie Leo wil men een tabel afdrukken met links de hoeveelheid in gram en rechts de prijs. De tabel ziet er zo uit:
 * gewicht		prijs
 * 50			0.5
 * 100			1.0
 * 150			1.5
 * 200			2.0
 * ...			...
 * 1000			10.0
 */
public class Oef04 {

	public static void main(String[] args) {
		double prijs;
		for (int gewicht = 50; gewicht <= 1000; gewicht += 50) {
						prijs = gewicht / 100.0;
						System.out.println(gewicht + "\t" + "\t" + prijs);
		}
	}

}
