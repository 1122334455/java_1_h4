/*
 * Geef een aantal getallen in via het toetsenbord. Druk het grootste en het kleinste getal af.
 * Het aantal getallen wordt op voorhand ingegeven. Je mag er vanuit gaan dat minstens 1 getal ingelezen wordt.
 */
public class Oef05 {

	public static void main(String[] args) {
		int getalAantal, getal = 0, groot = 0, klein = 0;
/*		int teller = 1;*/
		getalAantal = Invoer.leesInt("Geef het aan getallen in dat u zal ingeven: ");
		getal = Invoer.leesInt("Geef een getal in: ");
		groot = getal;
		klein = getal;
/*		while (teller < getalAantal) {
			getal = Invoer.leesInt("Geef een getal in: ");
			if (getal > groot) {
				groot = getal;
			} else if (getal < klein) {
				klein = getal;								
			}
			teller += 1; 
		}*/
		for (int i = 1; i <= (getalAantal - 1); i+=1) {
			getal = Invoer.leesInt("Geef een getal in: ");
			if (getal > groot) {
				groot = getal;
			} else if (getal < klein) {
				klein = getal;								
			}
		}
		System.out.println("Grootste getal: " + groot + "\t" + "Kleinste getal: " + klein);
		
	}
}
