/*
 * Idem als Extra Oef 3, maar gedurende een bepaalde periode. Deze eindigt als de invoer -1000 is.
 */
public class ExtraOef04 {

	public static void main(String[] args) {
		int dagTemp, totTemp = 0, teller = 0;
		double gemTemp;
		dagTemp = Invoer.leesInt("Geef de dagtemperatuur in: ");
		while (dagTemp != -1000) {
			totTemp += dagTemp;
			teller += 1;
			dagTemp = Invoer.leesInt("Geef de dagtemperatuur in: ");
		}
		gemTemp = (double)totTemp / teller;
		System.out.println("De gemiddelde temperatuur is: " + gemTemp + " graden");
		System.out.println(teller);
	}

}
