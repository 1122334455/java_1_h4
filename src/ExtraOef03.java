/*
 * Geef de dagtemperaturen via het toetsenbord in voor de maand januari, druk de gemiddelde temperatuur af.
 */
public class ExtraOef03 {

	public static void main(String[] args) {
		int dagTemp, totTemp = 0;
		double gemTemp;
		for (int i = 1; i <= 31; i++) {
			dagTemp = Invoer.leesInt("Geef de dagtemperatuur in: ");
			totTemp += dagTemp;
		}
		gemTemp = totTemp / 31.0;
		System.out.println("De gemiddelde maandtemperatuur is: " + gemTemp + " graden");
	}

}
