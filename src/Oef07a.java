/*
 * Via het toetsenbord wordt de naam van een student en zijn behaald percentage ingegeven, Doe een invoercontrole op het behaald percentage (enkel 0-100).
 * Telkens er een foute invoer is laat je een foutboodschap verschijnen (Fout! Minstens 0 en Fout! maximaal 100).
 * Bepaal de graad en druk deze samen met de naam af.
 * < 60%: onvoldoende, 60<70%: voldoende, 70<80%: onderscheiding, 80<85%: grote onderscheiding, >85%: grootste onderscheiding. 
 */
public class Oef07a {

	public static void main(String[] args) {
		String naamStud, graad = "";
		int percStud;
		naamStud = Invoer.leesString("Geef de naam in: ");
		percStud = Invoer.leesInt("Geef het percentage in: ");
		while (percStud < 0 || percStud > 100) {
			if (percStud < 0) {
				System.out.println("Fout! Het getal moet minstens 0 zijn!");
				percStud = Invoer.leesInt("Geef het percentage in: ");
			} else {
				System.out.println("Fout! Het getal max maximaal 100 zijn!");
				percStud = Invoer.leesInt("Geef het percentage in: ");
			}

		}
		if (percStud < 60) {
			graad = "Onvoldoende";
		} else if (percStud < 70) {
			graad = "Voldoende";
		} else if (percStud < 80) {
			graad = "Onderscheiding";
		} else if (percStud < 85) {
			graad = "Grote onderscheiding";
		} else {
			graad = "Grootste onderscheiding";
		}
		System.out.println("Naam: " + naamStud);
		System.out.println("Graad: " + graad);
	}
}