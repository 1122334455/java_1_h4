/*
 * Volgende gegevens worden ingevoerd: klantnr, naam, adres, woonplaats, handcode (0=klein, 1=groot), prov (L=Limburg). De invoer stopt bij klantnr 0.
 * Doe een invoercontrole op de handcode, geef een gepaste foutmelding.
 * Druk een lijst af van alle klanten in Limburg.
 * Druk het aantal Limburgse klanten af.
 * Druk het aantal Limburgse groothandelaars af.
 * Druk het percentage Limburgse groothandelaars tov het totaal aantal Limburgse klanten af.
 * Gebruik H4_extraoefening9.txt
 */
import java.io.*;

public class ExtraOef09 {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H4_extraoefening9.txt");
		String naam, adres, woonplaats, provCode;
		int klantNr, handCode, aantLim = 0, aantLGrootH = 0;
		double percGrootH;
		klantNr = a.leesInt();
		while (klantNr != 0) {
			naam = a.leesRegel();
			adres = a.leesRegel();
			woonplaats = a.leesRegel();
			handCode = a.leesInt();
			provCode = a.leesRegel();
			while (!(handCode == 0 || handCode == 1)) {
				System.out
						.println("Foutieve ingave! De handelaarscode moet 0 of 1 zijn!");
				handCode = Invoer.leesInt();
			}
			if (provCode.equals("L")) {
				aantLim += 1;
				System.out.printf("Klantnummer: %d\t\t Naam: %20s Adres: %30s Woonplaats: %15s Handelaarscode: %5s Provinciecode: %5s\n" , klantNr, naam, adres, woonplaats, handCode, provCode);
				if (handCode == 1) {
					aantLGrootH += 1;
				}
			}
			klantNr = a.leesInt();
		}	
		percGrootH = ((double) aantLGrootH / aantLim) * 100;
		System.out.println("\nAantal Limburgse klanten: " + aantLim);
		System.out.println("Aantal Limburgse groothandelaars: " + aantLGrootH);
		System.out.println("Het percentage groothandelaars in Limburg: "
				+ percGrootH);
	}

}
