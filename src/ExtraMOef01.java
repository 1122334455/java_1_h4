/*
 * Leonardo van Pisa, ook wel Fibonacci genoemd, bestudeerde omstreeks 1200 de vermenigvuldiging van konijnen. 
 * Hij gebruikte daarvoor een getallenrij waarin elke term gevormd wordt door de som van zijn twee directe voorgangers.
 * De eerste twee elementen zijn 1, de rij ziet er dus zo uit: 1 1 2 3 5 8 13 21 34 55 etc
 * Druk alle elementen af die kleiner zijn dan 1000. 
 */

public class ExtraMOef01 {

	public static void main(String[] args) {
		int getalA = 1, getalB = 1, som;
		System.out.println(getalA);
		System.out.println(getalB);
		som = getalA + getalB;
		while (som < 1000) {
			System.out.println(som);
			getalA = getalB;
			getalB = som;
			som = getalA + getalB;
		}

	}

} 
