/*
 * Er zijn 1000 gegevens, maak 10 partiele sommen elk na 100 gegevens. Druk deze af en maak de totaalsom.
 * Gebruik H4_oefening10.txt
 */
import java.io.*;

public class Oef10 {

	public static void main(String[] args) throws IOException {
		BestandsLezer b = new BestandsLezer("H4_Oefening10.txt");
		int getal = 0, som = 0, totSom = 0;
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 100; j++) {
				getal = b.leesInt();
				som += getal;
			}
			System.out.println(som);
			totSom += som;
			som = 0;
		}
		System.out.println("Totale som: " + totSom);
	}
}
