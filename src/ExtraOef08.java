/*
 * Je hebt 500 gegevens, maak 10 partiele sommen elk na 50 gegevens. Maak nadien de totale som. Gebruik H4_extraoefening8.txt
 */
import java.io.*;

public class ExtraOef08 {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H4_extraoefening8.txt");
		int data, parSom = 0, totSom = 0;
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 50; j++) {
				data = a.leesInt();
				parSom += data;
			}
			totSom += parSom;
			System.out.println("De partiele som is:" + parSom);
		}
		System.out.println("De totale som is: " + totSom);
	}

}
